#### Description
ToDo List implemented with <b>React + Redux</b> in which one can create new tasks, check them as completed or delete them<br>

#### Demo site
https://jprats89.gitlab.io/react-todo-list/

#### Run the project
To check the project just run from the root:

##### `npm start`

