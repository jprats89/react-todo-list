import React, {Component} from 'react';

export class Task extends Component {
  render() {
    let task = this.props.task;
    return (
      <p className={(task.checked) ? 'has-background-grey-lighter is-half box' : 'is-half box'}>
        {task.description}
        {(!task.checked)
          ? <span onClick={() => this.props.markTaskAsCompleted(task.id)} className="has-text-info ml-4 pointer">(Set as completed)</span>
          : <span className="has-text-success ml-4">DONE!</span>}
        <span onClick={() => this.props.removeTask(task.id)} className="delete is-pulled-right has-text-danger"></span>
      </p>
    )
  }
}