import React, {Component} from 'react';


export class FormNewTasks extends Component {
  state = {
    newTaskDescription: ''
  };

  _handlerChange = (e) => {
    this.setState({newTaskDescription: e.target.value})
  };

  render() {
    return (
      <form onSubmit={e => {
        e.preventDefault();
        this.props.addTask(this.state.newTaskDescription);
        this.setState({newTaskDescription: ''});
      }}>
        <div className="field is-grouped">
          <div className="control">
            <input value={this.state.newTaskDescription} onChange={this._handlerChange} type="text" className="input is-flat" placeholder="Create a todo list with Redux" />
          </div>
          <div className="control">
            <button type="submit" className="button is-link">
              <strong>Add task</strong>
            </button>
          </div>
        </div>
      </form>
    )
  }
}