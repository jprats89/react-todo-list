import React, {Component} from 'react';
import TaskContainer from '../container/Task'

export class List extends Component {
  render(){
    return (
      <div className="list-container">
        {this.props.list.map(task => (
          <TaskContainer key={task.description} task={task} />
        ))}
      </div>
    )
  }
}