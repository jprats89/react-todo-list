const INITIAL_STATE = [
  {id: 1, checked: false, description: 'Refactor controllers'},
  {id: 2, checked: false, description: 'Remove database unused tables'}
];

let id_actual_task = 2;

export function appReducer (state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'ADD_TASK':
      id_actual_task++;
      return state.concat([{id: id_actual_task,  checked: false, description: action.description}]);

    case 'REMOVE_TASK':
      id_actual_task--;
      return state.filter(task => task.id !== action.id);

    case 'MARK_AS_COMPLETED':
      return state.map(task => (task.id === action.id) ? {...task, checked: true} : task);

    default:
      return state;
  }
}