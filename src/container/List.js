import {connect} from 'react-redux';
import {List} from "../components/List";

const mapStateToProps = state => {
  return {list: state}
};

const createConnection = connect(mapStateToProps, null);
const ListContainer = createConnection(List);
export default ListContainer;
