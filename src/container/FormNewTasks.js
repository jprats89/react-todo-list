import {connect} from 'react-redux';
import {FormNewTasks} from '../components/FormNewTasks'
import { addTaskAction } from '../actions'

const mapStateToProps = null;

const mapDispatchToProps = dispatch => ({
  addTask: description => dispatch(addTaskAction(description))
});

const createConnection = connect(mapStateToProps, mapDispatchToProps);
const FormNewTasksContainer = createConnection(FormNewTasks);
export default FormNewTasksContainer;