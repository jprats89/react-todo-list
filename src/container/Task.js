import {connect} from 'react-redux';
import {Task} from "../components/Task";
import {removeTaskAction, markTaskAsCompletedAction} from "../actions";

const mapDispatchToProps = dispatch => ({
  removeTask: id => dispatch(removeTaskAction(id)),
  markTaskAsCompleted: id => dispatch(markTaskAsCompletedAction(id))
});

const createConnection = connect(null, mapDispatchToProps);
const TaskContainer = createConnection(Task);
export default TaskContainer;
