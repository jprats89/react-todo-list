export const addTaskAction = description => ({
  type: 'ADD_TASK',
  description
});

export const removeTaskAction = id => ({
  type: 'REMOVE_TASK',
  id
});

export const markTaskAsCompletedAction = id => ({
  type: 'MARK_AS_COMPLETED',
  id
});