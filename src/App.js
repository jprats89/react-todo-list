import React, {Component} from 'react';
import ListContainer from './container/List'
import FormNewTasksContainer from './container/FormNewTasks'
import {Provider} from 'react-redux'
import {createStore} from 'redux';
import {appReducer} from './reducers';

import './App.css';
import 'bulma/css/bulma.min.css'

const store = createStore(appReducer);

class App extends Component {
  render(){
    return (
      <Provider store={store}>
        <div className="main container">
          <h1 className="title">Todo List</h1>
          <FormNewTasksContainer />
          <ListContainer/>
        </div>
      </Provider>
    );
  }
}

export default App;
